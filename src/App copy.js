import React, { useState, useEffect } from 'react';
import { v4 as uuid } from 'uuid';
import localStorage from 'local-storage';
import './styles/App.css';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import DetalleTarea from './components/DetalleTarea.js';
import InputBuscador from './components/InputBuscador';
import MensajeNoEncontrado from './components/MensajeNoEncontrado';
import PendingTaskCounter from './components/PendingTaskCounter';
import CompletedTaskCounter from './components/CompletedTaskCounter';

function App() {
	const [resultadosEncontrados, setResultadosEncontrados] = useState(true);

	const storedTasks = localStorage.get('tasks') || [];
	const [tasks, setTasks] = useState(storedTasks);
	const [newTask, setNewTask] = useState({
		id: null,
		title: '',
		description: '',
		dueDate: null,
		priority: 'Normal',
		estado: 'pendiente', // Establece el estado como "pendiente" al agregar una nueva tarea
	});
	const [searchKeyword, setSearchKeyword] = useState('');
	const [mostrarModal, setMostrarModal] = useState(false);
	const [tareaSeleccionada, setTareaSeleccionada] = useState(null);
	const [pendingTaskCount, setPendingTaskCount] = useState(0);
	const [completedTaskCount, setCompletedTaskCount] = useState(0); // Contador de tareas terminadas

	useEffect(() => {
		// Calcula el contador de tareas terminadas cada vez que cambia la lista de tareas
		const completedTasks = tasks.filter(
			(task) => task.estado === 'terminado'
		);
		setCompletedTaskCount(completedTasks.length);
	}, [tasks]);

	useEffect(() => {
		localStorage.set('tasks', tasks);

		// Calcula el contador de tareas pendientes cada vez que cambia la lista de tareas
		const pendingTasks = tasks.filter((task) => task.estado === 'pendiente');
		setPendingTaskCount(pendingTasks.length);
	}, [tasks]);

	const agregarTarea = () => {
		if (newTask.title.trim() === '') return;
		newTask.id = uuid();
		setTasks([...tasks, newTask]);
		setNewTask({
			id: null,
			title: '',
			description: '',
			dueDate: null,
			priority: 'Normal',
			estado: 'pendiente', // Establece el estado como "pendiente" al agregar una nueva tarea
		});
	};

	const eliminarTarea = (taskId) => {
		const updatedTasks = tasks.filter((task) => task.id !== taskId);
		setTasks(updatedTasks);
	};

	const buscarTareas = (keyword) => {
		setSearchKeyword(keyword);

		const resultados = tasks.filter((task) =>
			task.title.toLowerCase().includes(keyword.toLowerCase())
		);

		setResultadosEncontrados(resultados.length > 0);
	};

	const mostrarDetalleTarea = (tarea) => {
		setTareaSeleccionada(tarea);
		setMostrarModal(true);
	};

	const cambiarEstado = () => {
		if (tareaSeleccionada) {
			const updatedTasks = tasks.map((task) =>
				task.id === tareaSeleccionada.id
					? {
							...task,
							estado:
								task.estado === 'pendiente' ? 'terminado' : 'pendiente',
					  }
					: task
			);
			setTasks(updatedTasks);
			setTareaSeleccionada({
				...tareaSeleccionada,
				estado:
					tareaSeleccionada.estado === 'pendiente'
						? 'terminado'
						: 'pendiente',
			});
		}
	};

	const cerrarModal = () => {
		setTareaSeleccionada(null);
		setMostrarModal(false);
	};

	return (
		<div className='ContainerPrincipal'>
			<h1 style={{ textAlign: 'center' }}>Mi Lista de Tareas</h1>
			<form onSubmit={agregarTarea} className='CuadroAgregar'>
				<input
					className='inputTitutoTarea'
					type='text'
					placeholder='Título de la tarea'
					value={newTask.title}
					onChange={(e) =>
						setNewTask({ ...newTask, title: e.target.value })
					}
				/>
				<textarea
					className='inputDescTarea'
					type='text'
					placeholder='Descripción de la tarea'
					value={newTask.description}
					onChange={(e) =>
						setNewTask({ ...newTask, description: e.target.value })
					}
				/>
				<div
					style={{
						display: 'flex',
						flexDirection: 'row',
						alignItems: 'flex-start',
						justifyContent: 'space-between',
					}}
				>
					<div
						style={{
							display: 'flex',
							flexDirection: 'column',
							marginRight: '40px',
						}}
						className='datePickerContainer'
					>
						<label className='datePickerLabel'>
							Fecha de vencimiento:
						</label>
						<DatePicker
							selected={newTask.dueDate}
							onChange={(date) =>
								setNewTask({ ...newTask, dueDate: date })
							}
							showTimeSelect
							timeFormat='HH:mm'
							timeIntervals={15}
							timeCaption='Hora'
							dateFormat='MMMM d, yyyy h:mm aa'
							placeholderText='Selecciona una fecha y hora'
						/>
					</div>
					<div
						style={{
							display: 'flex',
							flexDirection: 'column',
							marginRight: '40px',
						}}
					>
						<label className='datePickerLabel'>Prioridad:</label>
						<select
							className='inputTituloTarea'
							value={newTask.priority}
							onChange={(e) =>
								setNewTask({ ...newTask, priority: e.target.value })
							}
						>
							<option value='Urgente'>Urgente</option>
							<option value='Normal'>Normal</option>
							<option value='Poco Urgente'>Poco Urgente</option>
						</select>
					</div>
					<button
						className='btnAgregarTarea'
						type='submit'
						style={{ marginTop: '15px' }}
					>
						Agregar Tarea
					</button>
				</div>
			</form>

			<InputBuscador
				value={searchKeyword}
				onChange={(value) => buscarTareas(value)}
			/>

			<PendingTaskCounter count={pendingTaskCount} />

			<CompletedTaskCounter count={completedTaskCount} />

			{resultadosEncontrados ? (
				<ul className='ulListaTareas'>
					{tasks
						.filter((task) =>
							task.title
								.toLowerCase()
								.includes(searchKeyword.toLowerCase())
						)
						.map((task) => (
							<li
								className={`tareaListada ${
									task.estado === 'pendiente'
										? 'pendiente'
										: 'terminado'
								}`}
								key={task.id}
								onClick={() => mostrarDetalleTarea(task)}
							>
								<strong>Título:</strong> {task.title} <br />
								<strong>Descripción:</strong> {task.description} <br />
								<button
									className='btnEliminarTarea'
									onClick={() => eliminarTarea(task.id)}
								>
									Eliminar
								</button>
							</li>
						))}
				</ul>
			) : (
				<MensajeNoEncontrado />
			)}

			{mostrarModal && (
				<DetalleTarea
					tarea={tareaSeleccionada}
					onUpdateEstado={cambiarEstado}
					onClose={cerrarModal}
				/>
			)}
		</div>
	);
}

export default App;
