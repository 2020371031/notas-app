// routes/note.js
const express = require('express');
const router = express.Router();
const Note = require('../models/note');

// Ruta para crear una nueva nota
// Ruta para crear una nueva nota
router.post('/note', async (req, res) => {
	try {
		const newNote = new Note(req.body);
		const savedNote = await newNote.save();
		res.json(savedNote);
	} catch (error) {
		const errorMessage = 'Error al crear la nota';
		console.error(errorMessage, error); // Imprime el error en la consola para propósitos de depuración
		res.status(500).json({ error: errorMessage });
	}
});

// Ruta para obtener todas las notas
router.get('/note', async (req, res) => {
	try {
		const notes = await Note.find();
		if (notes.length === 0) {
			return res.status(404).json({ error: 'No se encontraron notas' });
		}
		res.json(notes);
	} catch (error) {
		console.error('Error al obtener las notas:', error);
		res.status(500).json({ error: 'Error al obtener las notas' });
	}
});

// Ruta para actualizar una nota
router.put('/note/:id', (req, res) => {
	Note.findByIdAndUpdate(req.params.id, req.body, { new: true })
		.then((data) => res.json(data))
		.catch((error) => res.json({ message: error }));
});

// Ruta para eliminar una nota
router.delete('/note/:id', (req, res) => {
	Note.findByIdAndRemove(req.params.id)
		.then((data) => res.json(data))
		.catch((error) => res.json({ message: error }));
});

module.exports = router;
