// models/note.js
const mongoose = require('mongoose');

const noteSchema = new mongoose.Schema({
	title: String,
	description: String,
	dueDate: Date,
	priority: String,
	estado: String,
});

module.exports = mongoose.model('Note', noteSchema);
